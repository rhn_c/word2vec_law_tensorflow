import os
import re
import time

def main(path,resPath,logPath):
	dudCase = "see case no"
	with open(logPath, 'w+') as logF:
		with open(resPath,'w+') as resF:
			lastDir = ''
			for r,dirs,files in os.walk(path):
				#for the sake of letting the user know which top level data folder is being processed
				if len(lastDir.split('/'))<=1 or not lastDir.split('/')[2] == r.split('/')[2]:
					now = time.strftime('%X %x %Z')
					print "Reading: " + r + " [" + now + "]"
					lastDir = r

				#process each case file
				for fn in files:
					if (fn.lower().endswith('.html') and not fn.lower().endswith('_1.html')):

						fnPath = os.path.join(r,fn)	

						logF.write("Starting: " + fnPath + "\n")
						contents = []
						caseInfo = ['','','','']
						readFile(fnPath, contents, caseInfo)
						contentsStr = cleanStr(' '.join(contents))
						#skip cases files that just refer to another case and have no text to learn from
						if not contentsStr==dudCase:
							for it in caseInfo:
								logF.write(it)
								logF.write(", ")
							resF.write(contentsStr)
							resF.write("\n")
						logF.write("\nFinished: " + fnPath + "\n")


	print "DONE"

def readFile(path,contents,caseInfo):

	with open(path) as f:
		for line in f:

			#metadata relating to which case, where it was heard and when it was heard
			if line.find('<p class=\"court\">')>=0:
				caseInfo[0] = getAtrText(line)
			
			elif line.find('<p class=\"case\">')>=0 or line.find('<p class=\"parties\">')>=0:
				caseInfo[1] = getAtrText(line)

			elif line.find('<p class=\"caseno\">')>=0 or line.find('<p class=\"docket\">')>=0:
				caseInfo[2] = getAtrText(line)

			elif line.find('<p class=\"date\">')>=0:
				caseInfo[3] = getAtrText(line)

			#case info and debriefings
			elif (line.find('<p>')>=0 or 
				line.find('<p class=\"note\">')>=0 or
				line.find('<p class=\"prelim\">')>=0 or
				line.find ('<p class=\"indent\">')>=0 or 
				line.find('<p class=\"parties\">')>=0):
				contents.append(getAtrText(line))


#For extracting relevant text data in specific xhtml tags
#Intended for the specific formating of legal documents by Public Resource Org
def getAtrText(line):

	ret=''

	if line.find("<span class=")>=0:
		st = line.find('\">')+2
		end = max(line.find('</span>'),line.find('</p>'))
		ret = line[st:end]

	elif line.count('>')>0 and line.count('<')>0:
		st = line.find('>')+1
		end = line.rfind('<')
		ret =  line[st:end]
	return ret


#Takes in a text string that may contain unrestricted text and html tags, returns the cleaned lower case string
#with only alphabetical characters remaining (i.e no numbers, punctuation, white space, special characters etc)
#Will also remove any text found between angular brackets (usually html code in this context) and any unicode
#html character entities i.e &#XXXX;
def cleanStr(line):
	
	cleanTag =re.compile('<.*?>')
	cleanUni =re.compile('&#.*?;')
	line_ = re.sub(cleanTag,' ',line)
	line_ = re.sub(cleanUni,' ',line_)
	line_ = ''.join([c for c in line_ if str.isalpha(c) or str.isspace(c)]).strip()
	line_ = ' '.join(line_.split()).lower()
	return line_
	

if __name__ == '__main__':

	logF = "./logF.txt"			#Path to log file to track which files have been cleaned
	resPath = "./cleanedTextData.txt"	#Path to file in which cleaned data will be output
	root = "./data/"			#Root location at which the text data to be cleaned is located

	print "Processing case files found in: " + root
	print "Storing cleaned data in: " + resPath
	print "Storing logging information in " + logF
	main(root,resPath,logF)
