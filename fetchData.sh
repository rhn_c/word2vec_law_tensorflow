#!/bin/bash

dataDir=./data/

if [ ! -d "$dataDir" ]; then
	mkdir -p -- $dataDir
fi

 
#Download Hein data
for i in {1..8}; do

	caseDir="000${i}.f.cas/" 
	#if the case file does not exist in the data folder, fetch, unzip, and then remove the tarball
	if [ ! -d "$dataDir$caseDir" ]; then
		caseFile=${caseDir::-1}".tgz" 
		url="https://bulk.resource.org/courts.gov/hein/${caseFile}"
		wget $url -P $dataDir
		tar -xzf $dataDir$caseFile -C $dataDir
	fi
done

#Download federal case data
for i in {2..3}; do

	fedDir="F${i}/"
	if [ ! -d "$dataDir$fedDir" ]; then
		fedFile="F${i}.tar.bz2"
		furl="https://bulk.resource.org/courts.gov/c/${fedFile}"
		wget $furl -P $dataDir
		tar -jxf $dataDir$fedFile -C $dataDir
	fi
done


#Download Supreme court case data
if [ ! -d "${dataDir}US/" ]; then
	usFile="US.tar.bz2"
	uurl="https://bulk.resource.org/courts.gov/c/${usFile}"
	wget $uurl -P $dataDir
	tar -jxf $dataDir$usFile -C $dataDir
fi

#Cleanup
rm -f "$dataDir"/*.tgz
rm -f "$dataDir"/*.bz2

